$(document).ready(function(){
$("#opac").css('height',$(document).height());
$(".modal").css('left',($(document).width() - 800) / 2);

//récupérer la longueur totale du document on la divise pour 
// avoir le milieu de la page et le -500 on retire la longueur du div 
// pour avoir le bon milieu
//****//
$("a.modal-active").click(function(){
	$("#opac").fadeIn("low");
	$(".modal").fadeIn("low");
});

$("#opac,#exit,#croix").click(function(){
	$("#opac").fadeOut("low");
	$(".modal").fadeOut("low");
	});
});