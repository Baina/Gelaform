(function(){


	/* 
	LORSQUE L'ON CLIQUE SUR UN ONGLET 
	on retire la class active de l'onglet actif
	J'ajoute la class active à l'onglet actuel 

	ON retire la class active sur le contenu actif 
	J'ajoute la class active sur le contenu correspondante


	*/

		var afficherOnglet = function (a,animations){
			if(animations === undefined){

				animations = true 
			}

		var li = a.parentNode

		var div = a.parentNode.parentNode.parentNode
		// Contenu actif
		var activeTab = div.querySelector('.tab-content.active')
		// Contenu à afficher
		var aAfficher = div.querySelector(a.getAttribute('href'))

		if(li.classList.contains('active')){
			return false
		}
		// ON retire la class active sur le contenu actif
		div.querySelector('.tabs .active').classList.remove('active')
		// J'ajoute la class active à l'onglet actuel 
		li.classList.add('active')
		// ON retire la class active sur le contenu actif 
		//div.querySelector('.tab-content.active').classList.remove('active')
		// J'ajoute la class active sur le contenu correspondante
		//div.querySelector(a.getAttribute('href')).classList.add('active')

		if (animations){

		activeTab.classList.add('fade')
		activeTab.classList.remove('in')
		var transitionend = function () {
			this.classList.remove('fade')
			this.classList.remove('active')
			aAfficher.classList.add('active')
			aAfficher.classList.add('fade')	
			aAfficher.offsetWidth		
			aAfficher.classList.add('in')
			activeTab.removeEventListener('transitionend', transitionend)
		}
		activeTab.addEventListener('transitionend', transitionend )
		activeTab.addEventListener('webkitTransionend', transitionend )
		activeTab.addEventListener('oTransitionEnd', transitionend )
	}else{
		aAfficher.classList.add('active')
		activeTab.classList.remove('active')
		}

		// On ajoute la class fade sur l'élement actif
		// A la fin de l'animation 
		// 	On retire la class fade et active
		// 	On ajoute la class active et fade a l'élement afficher
		//	On ajoute la class in

}	
	var tabs = document.querySelectorAll('.tabs a')
	for (var i = 0; i<tabs.length; i++){
	tabs[i].addEventListener('click', function (e){
		afficherOnglet(this)	

	})

}

	/* 
	JE RECUPERE LE HASH
	AJOUTER LA CLASS active sur le lien href="hash"
	RETIRER LA CLASS active sur les autres onglets
	AFFICHER / Masquer les contenus
	*/


	var hashChange = function (e) {

	var hash = window.location.hash
	var a = document.querySelector('a[href="' + hash +'"]')
	if (a !== null && !a.classList.contains('active')) {
		afficherOnglet(a, e !== undefined)
		}
}
	window.addEventListener('hashchange', hashChange)
	hashChange()

})()