// Vérification si Jquery est bien chargé
// Si c'est le cas lorsqu'on fait une action avec le clavier
// Dans le champ CP dans le champ codepostal portant la classe cp 
// Cette méthode va avoir une condition 
// Si le champ comporte 5 char on rentre dans la condition 
// Sinon on remet à zéro 
// Dans le cas où on rentre le code postal 
//On rentre dans la méthode Ajax
// Type get car on fait appel à une URL 
// On lui passe la valeur saisi dans le CP 
// Beforesend doit afficher l'icone chargement 
// Si déjà présente il ne l'affichera pas (pour éviter plusieurs icones chargement)
// Data c'est la valeur qui a été retourné par notre méthode (Json)
//On récupére data ville (lieu)
$("document").ready(function(){
$(".input_postal").keyup(function(){
        if ($(this).val().length === 5){

        $.ajax({
            type : 'get',
            //url: Routing.generate('Villes_France', {cp: $(this).val()}),
            url: 'http://localhost/Symfony2/web/app_dev.php/villes/'+ $(this).val(),
            
            beforeSend: function(){
                if ($(".chargement").length == 0) {
            $("form .input_lieu").parent().append('<div class="chargement"></div>');
            }
            // clear les options
            },
            success: function(data) {
/*                $.each(data, function(index, value) {
                $(".input_lieu").append($('<option>',{ value : value, text: value }));
                });*/
                $(".input_lieu").val(data);
                $(".chargement").remove();

            }

        });

    } else {
        $(".input_lieu").val('');
    }
    });

});