<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="theme")
 * @ORM\Entity
 */
class Theme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDemand", type="date", nullable=false)
     */
    private $datedemand;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleFormation", type="text", nullable=false)
     */
    private $libelleformation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="theme", type="date", nullable=false)
     */
    private $theme;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="date", nullable=false)
     */
    private $datefin;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptif", type="text", nullable=false)
     */
    private $descriptif;

    /**
     * @var integer
     *
     * @ORM\Column(name="codePostal", type="integer", nullable=false)
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=false)
     */
    private $lieu;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrePlace", type="integer", nullable=false)
     */
    private $nbreplace;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedemand
     *
     * @param \DateTime $datedemand
     *
     * @return Theme
     */
    public function setDatedemand($datedemand)
    {
        $this->datedemand = $datedemand;

        return $this;
    }

    /**
     * Get datedemand
     *
     * @return \DateTime
     */
    public function getDatedemand()
    {
        return $this->datedemand;
    }

    /**
     * Set libelleformation
     *
     * @param string $libelleformation
     *
     * @return Theme
     */
    public function setLibelleformation($libelleformation)
    {
        $this->libelleformation = $libelleformation;

        return $this;
    }

    /**
     * Get libelleformation
     *
     * @return string
     */
    public function getLibelleformation()
    {
        return $this->libelleformation;
    }

    /**
     * Set theme
     *
     * @param \DateTime $theme
     *
     * @return Theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return \DateTime
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     *
     * @return Theme
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin
     *
     * @param \DateTime $datefin
     *
     * @return Theme
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set descriptif
     *
     * @param string $descriptif
     *
     * @return Theme
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;

        return $this;
    }

    /**
     * Get descriptif
     *
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Set codepostal
     *
     * @param integer $codepostal
     *
     * @return Theme
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal
     *
     * @return integer
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Theme
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set nbreplace
     *
     * @param integer $nbreplace
     *
     * @return Theme
     */
    public function setNbreplace($nbreplace)
    {
        $this->nbreplace = $nbreplace;

        return $this;
    }

    /**
     * Get nbreplace
     *
     * @return integer
     */
    public function getNbreplace()
    {
        return $this->nbreplace;
    }
}
