<?php 


namespace AppBundle\Form;

use AppBundle\Form\themeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


class StagiaireType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		var_dump(__CLASS__." ".__FUNCTION__);
		//Formulaire d'inscription d'une nouvelle formation
		$builder

			->add('Lieu')
			->add('libelleFormation',TextType::class,
				array('label'=>'Libellé de la formation'))

			->add('dateDebut',DateType::class,
				array('label'=>'Début de la formation'))

			->add('dateFin',DateType::class,
				array('label'=>'Fin de la formation'))

			->add('descriptif',TextareaType::class,
				array('label'=>'Descriptif'))

			->add('codePostal',NumberType::class,
				array('label'=>'Code postal'))

			->add('lieu')
			->add('nbrePlace',NumberType::class,
				array('label'=>'Nombre de place'))

			->add('Valider', SubmitType::class, array('attr'=> array('label'=>'Valider')))
			->add('Enregistrer',SubmitType::class)
			->getForm();
	
	}

	public function getName()
	{
		return 'src_userbundle_formulaire';
	}


}