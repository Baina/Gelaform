<?php 


namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FormateurType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		var_dump(__CLASS__." ".__FUNCTION__);
		var_dump(__FUNCTION__);
		 
		// Ici permettra de faire l'inscription du formulaire
		$builder
			->add('nom',TextType::class)
			->add('prenom',TextType::class)
			->add('Date_de_naissance',BirthdayType::class)
														 '1'=>'Femme'),'expanded'=> true))
			->add('Pays','country')
			->add('Commune')
			->add('Nationalite','choice',array('choices'=>array('0'=>'Française',
														'1'=>'Etranger')))
			->add('email','email')
			->add('Valider', SubmitType::class, array('attr'=> array('class'=>'Valider')))
			->getForm();



}
	public function getName()
	{
		return 'src_userbundle_formulaire';
	}

}