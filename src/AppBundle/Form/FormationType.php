<?php 


namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class FormationType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$date = new \DateTime();
		/*var_dump(__CLASS__." ".__FUNCTION__);*/
		//Formulaire d'inscription d'une nouvelle formation
		$builder
			->add('dateDemand','date',array(
				'label'=>'Date de la demande',
				'widget' => 'single_text',
				'attr' => array('class' => 'input_date'),
				'data'=> $date
			))

			->add('libelleFormation',TextType::class,
				array('label'=>'Libellé de la formation'))

			->add('theme',EntityType::class,
				array(
					'label'=>'Thème',
			   		 'class' => 'AppBundle:Theme',
			   		 'attr' => array(
			   		 'class' => 'input_theme'),
					 'query_builder' => function (EntityRepository $er) {
					 
              			 $qb = $er->createQueryBuilder('f');

              			return $qb;
        
          			 }
          		))

			->add('dateDebut','date',
				array(
					'label'=>'Début de la formation',
                    'widget' => 'single_text',
                    'attr' => array('class' => 'datepicker input_date')
				))

			->add('dateFin','date',
				array(
					'label'=>'Fin de la formation',
                    'widget' => 'single_text',
                    'attr' => array('class' => 'datepicker input_date')
                ))	

			->add('descriptif',null,
				array(
					'label'=>'Description',))

			->add('codePostal',NumberType::class,
				array(
					'label'=>'Code postal',
					'attr' => array('class' => 'input_postal')
					))

			->add('lieu',TextType::class,
				array(
					'attr' => array(
					'class' => 'input_lieu'),
					'label'=>'Lieu'))

			->add('nbrePlace',NumberType::class,
				array(
					'label'=>'Nombre de place',
					'attr' => array('class' => 'input_Nbplace')
					))

			->add('Valider', SubmitType::class, 
				array(
					'attr'=> array(
					'class'=> 'input_envoie',
					'label'=>'Valider')
					))
			->getForm();
	
	}

	public function getName()
	{
		return 'src_userbundle_formulaire';
	}


}