<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Acl\Model\SecurityIdentityInterface;
use Symfony\Component\Security\Core\Role\Role;


/**
 * Formation
 *
 * @ORM\Table(name="formation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDemand", type="date")
     */
    
 
    private $dateDemand;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleFormation", type="string", length=255)
     *
    */
    private $libelleFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="text")
     */
    private $theme;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="date")
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="Descriptif", type="text")
     */
    private $descriptif;

    /**
     * @var int
     *
     * @ORM\Column(name="codePostal", type="integer")
     */
    private $codePostal;

    /**
     * @var integer
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrePlace", type="integer")*
     */
    private $nbrePlace;

    private $user;



/**
 * Get user id
 * @return integer $userId
 */
protected function getUserId()
{
    $user = $this->get('security.context')->getToken()->getUser();
    $userId = $user->getId();
    return $userId;
}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDemand
     *
     * @param \DateTime $dateDemand
     * @return AddFormation
     */
    public function setDateDemand($dateDemand)
    {
        $this->dateDemand = $dateDemand;

        return $this;
    }

    /**
     * Get dateDemand
     *
     * @return \DateTime 
     */
    public function getDateDemand()
    {
        return $this->dateDemand;
    }

    /**
     * Set libelleFormation
     *
     * @param string $libelleFormation
     * @return AddFormation
     */
    public function setLibelleFormation($libelleFormation)
    {
        $this->libelleFormation = $libelleFormation;

        return $this;
    }

    /**
     * Get libelleFormation
     *
     * @return string 
     */
    public function getLibelleFormation()
    {
        return $this->libelleFormation;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return AddFormation
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return AddFormation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return AddFormation
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set descriptif
     *
     * @param string $descriptif
     * @return AddFormation
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;

        return $this;
    }

    /**
     * Get descriptif
     *
     * @return string 
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     * @return AddFormation
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer 
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return AddFormation
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set nbrePlace
     *
     * @param integer $nbrePlace
     * @return AddFormation
     */
    public function setNbrePlace($nbrePlace)
    {
        $this->nbrePlace = $nbrePlace;

        return $this;
    }

    /**
     * Get nbrePlace
     *
     * @return integer 
     */
    public function getNbrePlace()
    {
        return $this->nbrePlace;
    }

    public function __toString()
    {
        return $this->getlibelleFormation();
        return $this->getDateFin();
        return $this->getDateDebut();
    }


}



