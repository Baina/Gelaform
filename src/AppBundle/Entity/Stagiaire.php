<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stagiaire
 *
 * @ORM\Table(name="stagiaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StagiaireRepository")
 */
class Stagiaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lieu", type="integer")
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var int
     *
     * @ORM\Column(name="licence_ref", type="integer", length=20)
     */
    private $licenceRef;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_stagiaire", type="string", length=255)
     */
    private $nomStagiaire;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_stagiaire", type="string", length=255)
     */
    private $prenomStagiaire;

    /**
     * @var string
     *
     * @ORM\Column(name="adr_stagiaire", type="string", length=255)
     */
    private $adrStagiaire;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_stagiaire", type="string", length=255)
     */
    private $villeStagiaire;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_assurance", type="string", length=255)
     */
    private $statutAssurance;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieu
     *
     * @param integer $lieu
     *
     * @return Stagiaire
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return int
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Stagiaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Stagiaire
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set licenceRef
     *
     * @param integer $licenceRef
     *
     * @return Stagiaire
     */
    public function setLicenceRef($licenceRef)
    {
        $this->licenceRef = $licenceRef;

        return $this;
    }

    /**
     * Get licenceRef
     *
     * @return int
     */
    public function getLicenceRef()
    {
        return $this->licenceRef;
    }

    /**
     * Set nomStagiaire
     *
     * @param string $nomStagiaire
     *
     * @return Stagiaire
     */
    public function setNomStagiaire($nomStagiaire)
    {
        $this->nomStagiaire = $nomStagiaire;

        return $this;
    }

    /**
     * Get nomStagiaire
     *
     * @return string
     */
    public function getNomStagiaire()
    {
        return $this->nomStagiaire;
    }

    /**
     * Set prenomStagiaire
     *
     * @param string $prenomStagiaire
     *
     * @return Stagiaire
     */
    public function setPrenomStagiaire($prenomStagiaire)
    {
        $this->prenomStagiaire = $prenomStagiaire;

        return $this;
    }

    /**
     * Get prenomStagiaire
     *
     * @return string
     */
    public function getPrenomStagiaire()
    {
        return $this->prenomStagiaire;
    }

    /**
     * Set adrStagiaire
     *
     * @param string $adrStagiaire
     *
     * @return Stagiaire
     */
    public function setAdrStagiaire($adrStagiaire)
    {
        $this->adrStagiaire = $adrStagiaire;

        return $this;
    }

    /**
     * Get adrStagiaire
     *
     * @return string
     */
    public function getAdrStagiaire()
    {
        return $this->adrStagiaire;
    }

    /**
     * Set villeStagiaire
     *
     * @param string $villeStagiaire
     *
     * @return Stagiaire
     */
    public function setVilleStagiaire($villeStagiaire)
    {
        $this->villeStagiaire = $villeStagiaire;

        return $this;
    }

    /**
     * Get villeStagiaire
     *
     * @return string
     */
    public function getVilleStagiaire()
    {
        return $this->villeStagiaire;
    }

    /**
     * Set statutAssurance
     *
     * @param string $statutAssurance
     *
     * @return Stagiaire
     */
    public function setStatutAssurance($statutAssurance)
    {
        $this->statutAssurance = $statutAssurance;

        return $this;
    }

    /**
     * Get statutAssurance
     *
     * @return string
     */
    public function getStatutAssurance()
    {
        return $this->statutAssurance;
    }
}

